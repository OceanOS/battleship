#ifndef PC_H_
#define PC_H_
#include "player.h"
class pc : public player {
public:
  static pc& instance()
  {
    static pc instance;
    return instance;
  }
  int place_boats();
  void attack_position();
  int attack_position(int x, int y);
private:
  pc();
  pc(pc const&);
  void operator=(pc const&);
};

#endif
