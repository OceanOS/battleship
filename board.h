#ifndef BOARD_H_
#define BOARD_H_

#include "boat.h"

#define TERM_X 80
#define TERM_Y 21

#define BOARD_X 10
#define BOARD_Y 10

typedef enum terrain_type {
  TERR_PATROL,
  TERR_FRIGATE,
  TERR_SUBMARINE,
  TERR_BATTLESHIP,
  TERR_CARRIER,
  TERR_WATER,
  TERR_COUNT
} terrain_type_t;

class board {
public:
  board();
  boat* player_grid[BOARD_Y][BOARD_X];
  terrain_type_t player_map[BOARD_Y][BOARD_X];
  char guess_grid[BOARD_Y][BOARD_X];
  void place_boat(int x, int y, boat* b);
  void disp_board();
};

#endif
