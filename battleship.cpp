#include <iostream>
#include <cstdlib>
#include <ctime>
#include <ncurses.h>
#include "pc.h"
#include "opponent.h"
#include "io.h"
#include <unistd.h>
int boat::pegs[];

void init_pegs() 
{
  boat::pegs[BOAT_PATROL] = 2;
  boat::pegs[BOAT_FRIGATE] = 3;
  boat::pegs[BOAT_SUBMARINE] = 3;
  boat::pegs[BOAT_BATTLESHIP] = 4;
  boat::pegs[BOAT_CARRIER] = 5;
}


int main(int argc, char* argv[])
{
  srand(time(NULL));
  init_pegs();
  player* players[2] = {&pc::instance(), &opponent::instance()};

  player* p = players[0];
  io_init();
  while (opponent::instance().place_boats());

  io_display(p);
  while(p->place_boats());
  io_clear_head();
  io_display(p);
  int p_switch = 0;
  while(p->player_win_status == win_battling && !pc::instance().quit)
  {
    p->attack_position();
    io_display(&pc::instance());
    io_display_player_status(&pc::instance());
    refresh();
    p_switch++;
    p_switch = p_switch % 2;
    p = players[p_switch];
  }
  //getch();
  //delete p; 
  io_reset();
  if (pc::instance().player_win_status == win_win)
    std::cout << "You won!" << std::endl;
  else
    std::cout << "You lost!" << std::endl;
  return 0;
}
