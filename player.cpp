#include "player.h"
#include <cstdlib>

player::player()
{
  boats.push_back(new boat(BOAT_PATROL));
  boats.push_back(new boat(BOAT_FRIGATE));
  boats.push_back(new boat(BOAT_SUBMARINE));
  boats.push_back(new boat(BOAT_BATTLESHIP));
  boats.push_back(new boat(BOAT_CARRIER));

  quit = false;
  player_win_status = win_battling; 
}

player::~player()
{
  for (int i = 0; i < BOAT_COUNT; i++)
  {
    delete boats[i];
  }
}

void player::clear_board()
{
  for (int i = 0; i < 10; i ++)
  {
    for (int j = 0; j < 10; j ++)
    {
      player_board.player_grid[i][j] = NULL;
      player_board.player_map[i][j] = TERR_WATER;
    }
  }
}


win_state_t determine_player_win_status(player *p)
{
  for (std::vector<boat*>::iterator it = p->boats.begin(); it != p->boats.end(); ++it)
    if ((*it)->alive == 1)
      return win_battling;
  p->player_win_status = win_loss;
  return win_win;
}

