#include "board.h"
#include <iostream>

board::board()
{
  for (int i = 0; i < 10; i ++){
    for (int j = 0; j < 10; j++){
      player_map[i][j] = TERR_WATER;
      guess_grid[i][j] = '*';
    }
  }
}

void board::place_boat(int x, int y, boat *b)
{
  b->position[dim_x] = x;
  b->position[dim_y] = y;
  if (b->direction == DIR_VERTICAL)
  {
    for (int i = 0; i < boat::pegs[b->type]; i ++) 
    {
      player_map[y+i][x] = (terrain_type_t)b->type;
      player_grid[y+i][x] = b; 
    }
  } else { // we'll never have to deal with one accidently being the type "dir_count"
    for (int i = 0; i < boat::pegs[b->type]; i ++)
    {
      player_map[y][x+i] = (terrain_type_t)b->type;
      player_grid[y][x+i] = b;
    }
  }
}

void board::disp_board()
{
  for (int i = 0; i < 10; i++)
  {
    for (int j = 0; j < 10; j++)
    {
      if (player_map[i][j] == TERR_WATER){
        std::cout << "* ";
      }else{
        std::cout << player_grid[i][j]->symbol << " ";
      }
    }
    std::cout << std::endl;
  }
}
