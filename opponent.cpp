#include "opponent.h"
#include "pc.h"
#include "io.h"
#include <ncurses.h>
#include <cstdlib>
#include <algorithm>


opponent::opponent()
{
  for (int i = 0; i < BOAT_COUNT; i ++)
  {
    boat_direction_t dir = (boat_direction_t)(rand() % 2);
    boats[i]->direction = dir;
  }

  coord_t c;
  for (c.y = 0; c.y < 10; c.y++)
  {
    for (c.x = 0; c.x < 10; c.x++)
    {
      remaining_coords.push_back(c);
    }
  }
}

int opponent::place_boats()
{
  pair_t p;
  // TODO: fix the segfault
  for (std::vector<boat*>::iterator it = boats.begin(); it != boats.end(); ++it)
  {
    bool is_horiz = (*it)->direction == DIR_HORIZONTAL;
    (*it)->position[dim_x] =  rand() % (10 - (is_horiz ? (*it)->length : 0));
    (*it)->position[dim_y] =  rand() % (10 - (is_horiz ? 0 : (*it)->length));
    for (p[dim_y] = (*it)->position[dim_y]; p[dim_y] <= (*it)->position[dim_y] + (is_horiz ? 0 : (*it)->length); p[dim_y]++)
    {
      for (p[dim_x] = (*it)->position[dim_x]; p[dim_x] <= (*it)->position[dim_x] + (is_horiz ? (*it)->length : 0); p[dim_x]++)
      {
        if (player_board.player_map[p[dim_y]][p[dim_x]] < TERR_WATER)
        {
          clear_board();
          //delete *it;
          return 1;
          //it = boats.begin();
        }
      }
    } 
    player_board.place_boat((*it)->position[dim_x], (*it)->position[dim_y], (*it));
  }

  return 0;

}

int opponent::attack_position(int x, int y)
{
  boat* target = NULL;
  if (pc::instance().player_board.player_map[y][x] >= TERR_WATER)
  {
    player_board.guess_grid[y][x] = 'o';
    return 0;
  }
  // we've successfully targeted a boat
  target = pc::instance().player_board.player_grid[y][x];
  bool is_offset_x = target->direction == DIR_HORIZONTAL;
  pair_t offset;
  offset[dim_x] = is_offset_x ? x - target->position[dim_x] : 0;
  offset[dim_y] = is_offset_x ? 0 : y - target->position[dim_y];
  // get which peg to mutate
  int dist = offset[dim_x] + offset[dim_y];
  target->health[dist] = 0;
  // see if the boat is sunk (aka not alive)
  int total_health = 0;
  for (int i = 0; i < target->length; i ++)
  {
    total_health += target->health[i];
  }
  target->alive = total_health != 0;

  // check if the pc's boats are all still afloat
  if (!target->alive)
    player_win_status = determine_player_win_status(&pc::instance());

  player_board.guess_grid[y][x] = 'x';
  return 1;
}

void opponent::attack_position()
{
  int index = rand() % remaining_coords.size();
  coord_t c = remaining_coords[index];
  remaining_coords.erase(remaining_coords.begin() + index);
  int success = attack_position(c.x, c.y); 
  if (success){
    io_clear_head();
    mvprintw(1,0,"The opponent hit your boat!");
  } else {
    io_clear_head();
    mvprintw(1,0,"The opponent missed!");
  }
}
