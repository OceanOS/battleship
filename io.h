#ifndef IO_H_
#define IO_H_
#include "dims.h"
class player;

void io_init(void);
void io_reset(void);
void io_display(player *p);
void io_handle_input(player *p);
void io_select_position(player *p, bool attack, pair_t dest);
void io_display_player_status(player *p);
void io_clear_head();
//void io_queue_message(const char *format, ...);

#endif
