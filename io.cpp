#include <ncurses.h>
#include <cstdlib>
#include <string>

#include "io.h"
#include "pc.h"
#include "opponent.h"

#define TARGET_HIT 1
void io_init(void)
{
  initscr();
  raw();
  noecho();
  curs_set(0);
  keypad(stdscr, TRUE);
  start_color();
  init_pair(TARGET_HIT, COLOR_RED, COLOR_BLACK);
}

void io_reset(void)
{
  endwin();
}

void io_display(player *p)
{

  // print coord labels
  for (int x = 0; x < 10; x++)
  {
    mvprintw(3,1+x*2,"%d",x);
    mvprintw(x+4,0,"%c", char('a'+x));
    mvprintw(3,24+x*2,"%d",x);
    mvprintw(x+4,23,"%c", char('a'+x));
  }

  // print player's active board
  mvprintw(2,1,"Your boats");
  for (int y = 0; y < 10; y++)
  {
    for (int x = 0; x < 10; x++)
    {
      if (p->player_board.player_map[y][x] == TERR_WATER)
      {
         mvaddch(y+4,1+x*2, '*');
      }
      else
      {
        boat* b = p->player_board.player_grid[y][x];
        bool is_offset_x = b->direction == DIR_HORIZONTAL;
        pair_t offset;
        offset[dim_x] = is_offset_x ? x - b->position[dim_x] : 0;
        offset[dim_y] = is_offset_x ? 0 : y - b->position[dim_y];
        int dist = offset[dim_x] + offset[dim_y];
        if (b->health[dist] == 0)
          attron(COLOR_PAIR(TARGET_HIT));
        mvaddch(y+4, 1+x*2, p->player_board.player_grid[y][x]->symbol);
        attroff(COLOR_PAIR(TARGET_HIT));
        b = NULL;
      }
    }
  }

  // print player's guess board
  mvprintw(2,24,"Your guesses");
  for (int y = 0; y < 10; y++)
  {
    for (int x = 0; x < 10; x++)
    {
      if (p->player_board.guess_grid[y][x] == 'x')
        attron(COLOR_PAIR(TARGET_HIT));
      mvaddch(y+4, 24+x*2, p->player_board.guess_grid[y][x]);
      attroff(COLOR_PAIR(TARGET_HIT));
    }
  }

  refresh();

}

void io_select_position(player *p, bool attack, pair_t dest)
{
  int x_bound = attack ? 24:1;
  int y_bound = 4;

  std::string place_boat = "choose a location to place your boat, then press 'p'\nthen press 'h' for a horizontal boat or 'v' for a vertical boat";
  std::string place_attack = "choose a location to fire!(press enter)";
  std::string pick = attack?place_attack:place_boat;
  mvprintw(0,0,pick.c_str());
  dest[dim_x] = 0;
  dest[dim_y] = y_bound;

  mvaddch(dest[dim_y],x_bound+ (2* dest[dim_x]), '+');
  refresh();
  int input;
  bool should_break = false;
  char key = attack? 10 : 'p';
  while (!should_break&&(input = getch()) != key )
  {
    io_display(p);
    //mvaddch(dest[dim_y], 1+ 2 * dest[dim_x], '+');
    switch(input)
    {
      case KEY_UP:
        if (dest[dim_y] > 4)
          dest[dim_y]--;
        break;
      case KEY_DOWN:
        if (dest[dim_y] < 13)
          dest[dim_y]++;
        break;
      case KEY_LEFT:
        if (dest[dim_x] > 0)
          dest[dim_x]--;
        break;
      case KEY_RIGHT:
        if (dest[dim_x] < 9)
          dest[dim_x]++;
        break;
      case 'q':
        p->quit = true;
        should_break = true;
        break;
      default:
        break;
    }
    attron(A_BOLD);
    mvaddch(dest[dim_y], x_bound +(2 * dest[dim_x]),'+');
    attroff(A_BOLD);
    refresh();
  }
  dest[dim_y] -= y_bound;
}

void io_display_player_status(player *p)
{
  mvprintw(15,1,"player boat status");
  int acc = 0;
  const int base_y = 16;
  const int base_x = 1;
  int off_x = 0;
  int off_y = 0;
  for (std::vector<boat *>::iterator it = p->boats.begin(); it != p->boats.end(); ++it)
  {
    off_x = base_x + 10 * (acc/2);
    off_y = acc % 2 == 0 ? 0 : 1;
    mvprintw(base_y + off_y,base_x + off_x, "%c:[",(*it)->symbol);
    for (int i = 0; i < (*it)->length; i++)
    {
      char stat = (*it)->health[i] == 1 ? '#' : 'x';
      if (stat == 'x')
        attron(COLOR_PAIR(TARGET_HIT));
      addch(stat);
      attroff(COLOR_PAIR(TARGET_HIT));
    }
    addch(']');
    acc++;
  }
  
}

void io_clear_head()
{
  // clear the top of the screen
  move(0,0);
  clrtoeol();
  move(1,0);
  clrtoeol();
}
