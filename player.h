#ifndef PLAYER_H_
#define PLAYER_H_

#include <vector>
#include "board.h"

typedef enum win_state {
  win_win,
  win_loss,
  win_battling
} win_state_t;

class player {
public:
  player();
  virtual ~player();
  board player_board;
  virtual int place_boats() = 0;
  virtual int attack_position(int x, int y) = 0;
  virtual void attack_position() = 0;;
  void clear_board();
  std::vector<boat*> boats;
  bool quit;
  win_state_t player_win_status;
};

// sets the win status of p to win_loss if p has no
// more boats afloat and returns win_win so that the
// player calling it may set their win status to 
// win_win.  this is pretty janky.
win_state_t determine_player_win_status(player *p);

#endif
