#ifndef DIMS_H_
#define DIMS_H_

typedef enum dim {
  dim_x,
  dim_y,
  num_dims
} dim_t;

typedef int pair_t[num_dims];

#endif
