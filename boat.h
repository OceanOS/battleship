#ifndef BOAT_H_
#define B0AT_H_

#include "dims.h"
typedef enum boat_direction {
  DIR_VERTICAL,
  DIR_HORIZONTAL,
  DIR_COUNT
} boat_direction_t;

typedef enum boat_type {
  BOAT_PATROL,
  BOAT_FRIGATE,
  BOAT_SUBMARINE,
  BOAT_BATTLESHIP,
  BOAT_CARRIER,
  BOAT_COUNT
} boat_type_t;

const char boat_symbols[5] = {'P','F','S','B','C'};

class boat{
public:
  static int pegs[BOAT_COUNT];
  bool alive;
  boat_type_t type;
  boat_direction_t direction;
  pair_t position;
  int* health;
  char symbol;
  int length;
  boat(boat_type_t bt) {
    type = bt;
    health = new int[pegs[bt]];
    symbol = boat_symbols[bt];
    for (int i = 0; i < pegs[bt]; i ++)
    {
      health[i] = 1;
    }
    alive = true;
    length = pegs[bt];
  }
  ~boat() { delete health; }
};



#endif
