#include <iostream>
#include <ncurses.h>
#include <cstdlib>
#include <string>
#include "opponent.h"
#include "io.h"
#include "pc.h"

std::string sink_msg[5];

pc::pc()
{
  sink_msg[BOAT_PATROL] = "You sunk their patrol boat";
  sink_msg[BOAT_FRIGATE] = "You sunk their frigate";
  sink_msg[BOAT_SUBMARINE] = "You sunk their submarine";
  sink_msg[BOAT_BATTLESHIP] = "You sunk their battleship";
  sink_msg[BOAT_CARRIER] = "You sunk their carrier";
}

int pc::place_boats()
{
  for (std::vector<boat*>::iterator it = boats.begin(); it != boats.end(); ++it)
  {
    bool valid_pos = false;
    pair_t input;
    bool horizontal = false;
    do{
     int x,y;
     x = y = -1;
     io_select_position(&pc::instance(), false, input);
     int dir = 0;
     // add a way of notifying the user
     while (dir != int('h') && dir != int('v'))
       dir = getch();
     horizontal = char(dir) == 'h';
     x = input[dim_x];
     y = input[dim_y];
     valid_pos = true;
     if ((horizontal && x + (*it)->length > 10) || 
         (!horizontal && y + (*it)->length > 10))
     {
       valid_pos = false;
       continue;
     }
     for (y = input[dim_y]; y < input[dim_y] + (horizontal ? 0 : (*it)->length); y++)
     {
       for (x = input[dim_x]; x < input[dim_x] + (horizontal ? (*it)->length:0); x++)
       {
         if (player_board.player_map[y][x] < TERR_WATER)
         {
           valid_pos = false;
         }
       } 
     } 
    }while(!valid_pos);
    (*it)->position[dim_y] = input[dim_y];
    (*it)->position[dim_x] = input[dim_x];
    (*it)->direction = horizontal ? DIR_HORIZONTAL : DIR_VERTICAL;
    player_board.place_boat(input[dim_x], input[dim_y], (*it));
    io_display(&pc::instance());
    refresh();
  }
  return 0;
}

int pc::attack_position(int x, int y)
{
  boat* target = NULL;
  if (opponent::instance().player_board.player_map[y][x] >= TERR_WATER)
  {
    player_board.guess_grid[y][x] = 'o';
    return 0;
  }
  // we've successfully targeted a boat
  target = opponent::instance().player_board.player_grid[y][x];
  bool is_offset_x = target->direction == DIR_HORIZONTAL;
  pair_t offset;
  offset[dim_x] = is_offset_x ? x - target->position[dim_x] : 0;
  offset[dim_y] = is_offset_x ? 0 : y - target->position[dim_y];
  // get which peg to mutate
  int dist = offset[dim_x] + offset[dim_y];
  target->health[dist] = 0;
  // see if the boat is sunk (aka not alive)
  int total_health = 0;
  for (int i = 0; i < target->length; i ++)
  {
    total_health += target->health[i];
  }
  target->alive = total_health != 0;

  // check if the opponent's boats are still afloat
  if (!target->alive) {
    std::string msg = sink_msg[target->type] + " (press any key to continue)";
    mvprintw(1,0, msg.c_str());
    getch();
    io_clear_head(); 
    player_win_status = determine_player_win_status(&opponent::instance());
  }

  player_board.guess_grid[y][x] = 'x';
  return 1;
}

void pc::attack_position()
{
  pair_t dest;
  io_select_position(&pc::instance(), true, dest);
  if (quit) return;
  int success =attack_position(dest[dim_x], dest[dim_y]);
  if (success) {
    io_clear_head();
    mvprintw(1,0,"You hit an enemy ship!");
  } else {
    io_clear_head();
    mvprintw(1,0,"You missed!");
  }
}

