#ifndef OPPONENT_H_
#define OPPONENT_H_

#include "player.h"
#include <vector>
// dirty hack: since vectors can't store arrays, I'm keeping track
// of the points the computer has not guessed yet by storing the
// points in a vector
typedef struct coord {
  int x;
  int y;
} coord_t;

class opponent : public player {
public:
  static opponent& instance()
  {
    static opponent instance;
    return instance;
  }
  int place_boats();
  void attack_position();
  int attack_position(int x, int y);
private:
  opponent();
  opponent(opponent const&);
  void operator=(opponent const&);
  std::vector<coord_t> remaining_coords;
};

#endif
